dataDir = "benan_Ses\"
ads = audioDatastore(dataDir, 'IncludeSubfolders', true,...
    'FileExtensions', '.wav',...
    'LabelSource','foldernames')
[trainDatastore, testDatastore]  = splitEachLabel(ads,0.80);
trainDatastoreCount = countEachLabel(trainDatastore)
[sampleTrain, info] = read(trainDatastore);
sound(sampleTrain,info.SampleRate)
reset(trainDatastore);
lenDataTrain = length(trainDatastore.Files);
features = cell(lenDataTrain,1);
% train verisini olusturma
for i = 1:lenDataTrain
    [dataTrain, infoTrain] = read(trainDatastore);
    features{i} = HelperComputePitchAndMFCC(dataTrain,infoTrain);
end
features = vertcat(features{:});
features = rmmissing(features);
featureVectors = features{:,2:15};
 % Normalize
m = mean(featureVectors);
s = std(featureVectors);
features{:,2:15} = (featureVectors-m)./s;
head(features)   

 % Train data knn
[trainedClassifier, validationAccuracy, confMatrix] = ...
    HelperTrainKNNClassifier(features);
 % heatmap
heatmap(trainedClassifier.ClassNames, trainedClassifier.ClassNames, ...
    confMatrix);
title('Confusion Matrix');

 % Test verisi olusturma
lenDataTest = length(testDatastore.Files);
featuresTest = cell(lenDataTest,1);
for i = 1:lenDataTest
  [dataTest, infoTest] = read(testDatastore);
  featuresTest{i} = HelperComputePitchAndMFCC(dataTest,infoTest);
end
featuresTest = vertcat(featuresTest{:});
featuresTest = rmmissing(featuresTest);
featuresTest{:,2:15} = (featuresTest{:,2:15}-m)./s;
head(featuresTest)   % Display the first few rows
 % knn
result = HelperTestKNNClassifier(trainedClassifier, featuresTest)
result

